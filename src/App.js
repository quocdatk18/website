
import './App.css';
import { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Header from './components/Header';
import Home from './components/Home';
import Product from './components/Product';
import Contact from './components/Add';
import Footer from './components/Footer';
import axios from 'axios';
import NotFound from './components/NotFound';
import { getProducts } from './services/api';
import Add from './components/Add';

function App() {
  const [products, setProducts] = useState([]);

  const getProductsAll = async () => {
    try {
      const response = await getProducts();
      setProducts(response.data);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    getProductsAll();
  }, [])
  const Deleteproduct = async id => {
    try {
      await axios.delete(`http://localhost:3002/products/${id}`)
      const newproduct = products.filter(item => item.id !== id)
      setProducts(newproduct)
    } catch (error) {
      console.log(error.message)

    }
  }

  const handleSearch = async (keyword) => {
    var response = await getProducts();
    var filterArr = response.data.filter(item => {
      return item.name.toLowerCase().includes(keyword.toLowerCase());
    });
    setProducts(filterArr);
  }

  return (
    <Router>
      <div className="App">
        <Header parentCallback={handleSearch} />

        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/product">
            <div className="container">
              <div className="row">
                {products.map(product1 => {
                  return <Product item={product1} key={product1.id} deleteProductFunc={Deleteproduct} />
                })}
              </div>
            </div>
          </Route>
          <Route path="/add" component={Add} />
          <Route component={NotFound} />
        </Switch>
      </div>

      <Footer />

    </Router>
  );
}

export default App;
