import React, { useState, useEffect } from 'react';
import { Form, Button } from 'bootstrap-4-react'
import AddProduct from './AddProduct';

import axios from 'axios';

export default function Add() {
    const [products, setProducts] = useState([]);
    const addProduct = async (name, description, price, img) => {
        try {
            const res = await axios.post(
                'http://localhost:3002/products',
                {
                    id: Math.random() * 100,
                    name,
                    description,
                    price,
                    img

                }
            )
            console.log(res.data)
            const newTodos = [...products, res.data]
            setProducts(newTodos)
        } catch (error) {
            console.log(error.message)
        }
    }

    return (
        <div>
            <AddProduct addProductFuc={addProduct} />
        </div>
    )
}
