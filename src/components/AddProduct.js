import React, { useState } from 'react'
import PropTypes from 'prop-types'


function AddProduct(props) {
    console.log(props)
    const addProduct = props.addProductFuc
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [img, setImg] = useState('')

    const hangdlesubmit = (e,) => {
        e.preventDefault()
        if (name !== "" && description !== "" && price !== "" && img !== "") {
            addProduct(name, description, price, img)
            setName('')
            setDescription('')
            setPrice('')
            setImg('')

        }
        window.location = "/product"
    }
    const changename = (e) => setName(e.target.value)
    const changeDescription = (e) => setDescription(e.target.value)
    const changePrice = (e) => setPrice(e.target.value)
    const changeImg = (e) => setImg(e.target.value)
    return (
        <div>
            <form onSubmit={hangdlesubmit}>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Name </label>
                    <input type="text" className="form-control" name="name" onChange={changename} id="exampleInputEmail1" aria-describedby="emailHelp" />

                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Description</label>
                    <input type="text" className="form-control" name="description" onChange={changeDescription} id="exampleInputPassword1" />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Price</label>
                    <input type="text" className="form-control" name="price" onChange={changePrice} id="exampleInputPassword2" />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Img  Url</label>
                    <input type="text" className="form-control" name="img" onChange={changeImg} id="exampleInputPassword3" />
                </div>

                <button type="submit" className="btn btn-primary">Add</button>
            </form>

        </div >
    )
}
AddProduct.propTypes = {
    addProductFuc: PropTypes.func.isRequired
}

export default AddProduct
