import React from 'react';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';



function Product(props) {
    const style = {
        minHeight: '200px'
    }
    const { name, img, description, price } = props.item
    const deleteProduct = props.deleteProductFunc;




    return (

        <div className="col col-md-3">
            <div className="card mt-4">

                <img src={img} alt="" style={style} />

                <div className="card-body">

                    <h5 className="card-title">Name: {name}</h5>
                    <p className="card-text">Description: {description}</p>
                    <p className="card-text">Price: {price}</p>
                </div>

            </div>
            <Button variant="contained" onClick={deleteProduct.bind(this, props.item.id)} color="secondary">
                Delete
            </Button>

        </div>

    )
}
Product.propTypes = {

    item: PropTypes.object,
    deleteProductFunc: PropTypes.func.isRequired
}

export default Product;