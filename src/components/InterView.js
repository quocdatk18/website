import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function SimpleSlider() {
    const styleimgSile = {
        width: '100%',
        height: '300px'
    }
    const settings = {
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        speed: 2000,
        autoplaySpeed: 3000,
        cssEase: "linear"
    };
    return (
        <div>
            <Slider {...settings}>
                <div>
                    <img src="https://cdn.cellphones.com.vn/media/ltsoft/promotion/s20fe-b-690x300_13_.png" style={styleimgSile} alt="" />
                </div>
                <div>
                    <img src="https://cdn.cellphones.com.vn/media/ltsoft/promotion/w4-v2-690x300_12_.png" style={styleimgSile} alt="" />
                </div>
                <div>
                    <img src="https://cdn.cellphones.com.vn/media/ltsoft/promotion/asss690-300-max.png" style={styleimgSile} alt="" />
                </div>
                <div>
                    <img src="https://cdn.cellphones.com.vn/media/ltsoft/promotion/IMG_20210902_150624_915.png" style={styleimgSile} alt="" />
                </div>
                <div>
                    <img src="https://cdn.cellphones.com.vn/media/ltsoft/promotion/s20fe-b-690x300_13_.png" style={styleimgSile} alt="" />
                </div>
                <div>
                    <img src="https://cdn.cellphones.com.vn/media/ltsoft/promotion/asss690-300-max.png" alt="" style={styleimgSile} />
                </div>
            </Slider>
        </div>
    );
}
